export const primary = '#3fc2f0';
export const primaryContrast = '#60a5c4';
export const primaryDark = '#2286a8';
export const primaryLight = '#b9e3f9';
export const textColor = '#fff';
export const textColorDarkMode = '#000';
