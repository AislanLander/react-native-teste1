import React from 'react';
import {StyleSheet} from 'react-native';
import VideoPlayer from 'react-native-video-controls';

export const VideoContainer = props => {
  return (
    <VideoPlayer
      source={{
        uri: props.videoUrl,
      }}
      ref={ref => {
        this.player = ref;
      }}
      onBuffer={this.onBuffer}
      onError={this.videoError}
      style={styles.backgroundVideo}
      disableBack={true}
    />
  );
};
const styles = StyleSheet.create({
  backgroundVideo: {
    height: '30%',
  },
});
