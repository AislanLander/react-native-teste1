import React, {Component} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {MainButton} from '../../components/buttons/mainButton';
import {VideoContainer} from '../../components/videoContainer';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.navigation.bind(this);
  }
  static get options() {
    return {
      topBar: {
        title: {
          text: 'Page 1',
        },
      },
    };
  }
  navigation = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'SecondScreen',
      },
    });
  };
  state = {
    //ECONOMIZAR RAM
    videoVisible: true,
  };
  componentDidMount() {
    // escuta eventos de navegacao
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentWillUnmount() {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  componentDidDisappear() {
    // escuta se o componente não é o topo do stack de navegacao atual
    this.setState({videoVisible: false});
  }
  componentDidAppear() {
    this.setState({videoVisible: true});
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          {this.state.videoVisible ? (
            <View style={styles.VideoContainer}>
              <VideoContainer
                videoUrl={
                  'https://d1rfq3h2na8ms8.cloudfront.net/editorial/2018/12-11/Rn8KKQBy-medium.mp4'
                }
              />
            </View>
          ) : null}

          <MainButton onPress={this.navigation} title={'Next Page'} />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  VideoContainer: {
    height: '65%',
    width: '80%',
  },
});
