import React, {useState} from 'react';
import {View, SafeAreaView, StyleSheet} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {MainButton} from '../../components/buttons/mainButton';
import {VideoContainer} from '../../components/videoContainer';
import {
  useNavigationComponentDidAppear,
  useNavigationComponentDidDisappear,
} from 'react-native-navigation-hooks';

// Fazendo a segunda tela como functional component a fim de demonstração
export const SecondScreen = props => {
  const [isFocused, setIsFocused] = useState(true);
  const navigation = () => {
    Navigation.push(props.componentId, {
      component: {
        name: 'ThirdScreen',
      },
    });
  };
  // desmonta ou monta o componenente de video, economia de memória e recursos.
  useNavigationComponentDidAppear(e => {
    setIsFocused(true);
  }, props.componentId);
  useNavigationComponentDidDisappear(e => {
    setIsFocused(false);
  }, props.componentId);
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        {isFocused ? (
          <View style={styles.VideoContainer}>
            <VideoContainer
              videoUrl={
                'https://d1rfq3h2na8ms8.cloudfront.net/editorial/2018/12-11/X4W11rnR-medium.mp4'
              }
            />
          </View>
        ) : null}
        <MainButton onPress={navigation} title={'Next Page'} />
      </View>
    </SafeAreaView>
  );
};

SecondScreen.options = {
  topBar: {
    title: {
      text: 'Page 2',
    },
  },
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  VideoContainer: {
    height: '65%',
    width: '80%',
  },
});
