/**
 * @format
 */
import {Navigation} from 'react-native-navigation';
import HomeScreen from './src/screens/home';
import {SecondScreen} from './src/screens/secondScreen';
import {ThirdScreen} from './src/screens/thirdScreen.js';

//Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
Navigation.registerComponent('HomeScreen', () => HomeScreen);
Navigation.registerComponent('SecondScreen', () => SecondScreen);
Navigation.registerComponent('ThirdScreen', () => ThirdScreen);
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'HomeScreen',
              passProps: {
                text: 'page  1',
              },
            },
          },
        ],
      },
    },
  });
});
